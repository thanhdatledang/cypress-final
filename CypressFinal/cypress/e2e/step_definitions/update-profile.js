import { Given, When, Then } from "@badeball/cypress-cucumber-preprocessor";
import ProfilePage from "../../pages/profilePage";

const LBL_USER_NAME = (userName) =>
  `//div[@data-test='users-route']//div[text()="${userName}"]`;

Given("I go to the Profile page", () => {
  ProfilePage.clickProfileImg();
  ProfilePage.clickViewProfile();
});

When("I click on Edit tags link", () => {
  ProfilePage.clickEditProfile();
});

When("I edit all information of user", () => {
  cy.fixture("update-profile").then((userFixture) => {
    ProfilePage.inputFirstName(userFixture.firstName);
    ProfilePage.inputLastName(userFixture.lastName);
    ProfilePage.inputUserName(userFixture.userName);
    ProfilePage.inputLocation(userFixture.location);
    ProfilePage.inputBio(userFixture.bio)
  });
});

When("I click the Update Account button", () => {
  ProfilePage.clickUpdateAccount();
});

When("I go to new profile {string}", (userName) => {
  ProfilePage.navigateProfilePage(userName);
});

Then("I observe that it will take me to the Profile page", () => {
  cy.fixture("update-profile").then((userFixture) => {
    cy.url().should("eq", `https://unsplash.com/@${userFixture.userName}`);
  });
});

Then("My full name is displayed as {string}", (fullName) => {
  cy.xpath(LBL_USER_NAME(fullName)).should("be.visible");
});

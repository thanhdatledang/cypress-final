import { Given, When, Then } from "@badeball/cypress-cucumber-preprocessor";
import { ApiConstants } from "cypress/constants/api-constants";
import { ApiCollectionHelpers, ApiPhotoHelpers, ApiUserHelpers } from "../../helpers/api-helpers";
import { LikedPhotoListPage } from "../../pages/likedphoto-list-page";

let randomPhotoId;
let expectLikedId = [];
let collectionId;
let collectionPhotoId;

Given("There is a private collection with two photos", () => {
  ApiCollectionHelpers.createACollection().then((response) => {
    collectionId = response.body.id.toString();
  });
  ApiPhotoHelpers.getRandomPhoto(2).then(function (response) {
    let respBody = response.body;
    cy.log(respBody);
    for (let i = 0; i < respBody.length; i++) {
      randomPhotoId = respBody[i].id.toString();
      ApiCollectionHelpers.addAPhotoToCollection(randomPhotoId, collectionId);
    }
  });
  
});


Given("I logged into the application", () => {
  cy.visit("/login");
  cy.login(Cypress.env("email"), Cypress.env("password"));
});

Given("I remove 1 photo from the collection", () => {
  ApiCollectionHelpers.removeAPhotoFromCollection(randomPhotoId, collectionId).then(function (response) {
    let respBody = response.body;
    cy.log(respBody);
  });
});


When("I go to collection page", () => {
  cy.visit("/collections/" + collectionId);
});


Then("the photo has been removed successfully from the collection", () => {
  CollectionPage.getPhotoCountInCollection().should('have.length', 1)
});

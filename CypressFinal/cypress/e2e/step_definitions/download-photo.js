import { Given, When, Then } from "@badeball/cypress-cucumber-preprocessor";
import { ApiPhotoHelpers } from "cypress/helpers/api-helpers";

When("I open a random photo", () => {
    HomePage.clickFirstImg()
})
When("I download this photo", () => {
    ApiPhotoHelpers.getRandomPhoto(1).then(function (response) {
        let respBody = response.body;
        cy.log(respBody);
        for (let i = 0; i < respBody.length; i++) {
          randomPhotoId = respBody[i].id.toString();
          ApiPhotoHelpers.dowloadRandomPhoto(randomPhotoId);
        }
      });
})
Then("I notice that the image is downloadable and the correct image has been saved", () => {
    let fileName = randomPhotoId + '.jpg';
    cy.downloadFile(photo_url, 'CypressFinal/downloads', fileName)
    cy.verifyDownload(fileName, {contains: true}) 
})
import { Given } from "@badeball/cypress-cucumber-preprocessor";
import { HomePage } from "../../pages/home-page";

Given("I click the first photo on home page", () => {
    HomePage.clickFirstImg()
})
When("I hover on icon user at the top left corner", () => {
    HomePage.clickIconUser()
})
When("I click the Follow button", () => {
    HomePage.clickFollowBtn()
})
Then("I observe button background color turn into white and button text turn into Following", () => {
    cy.get('button[title="Following"]').should("be.visible");
    cy.get('button[title="Following"]').click();
})
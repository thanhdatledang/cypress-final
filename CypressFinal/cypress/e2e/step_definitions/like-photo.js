import { Given, When, Then } from "@badeball/cypress-cucumber-preprocessor";
import { ApiPhotoHelpers, ApiUserHelpers } from "../../helpers/api-helpers";
import { LikedPhotoListPage } from "../../pages/likedphoto-list-page";

let randomPhotoId;
let expectLikedId = [];

Given("I logged into the application", () => {
  cy.visit("/login");
  cy.login(Cypress.env("email"), Cypress.env("password"));
});
Given("There are no photo in my liked photos list", () => {
  ApiUserHelpers.getLikedPhotoList(Cypress.env("username")).then((response) => {
    let respBody = response.body;
    for (let i = 0; i < respBody.length; i++) {
      randomPhotoId = respBody[i].id.toString();
      ApiPhotoHelpers.likeOrUnlikeAPhoto("DELETE", randomPhotoId);
    }
  });
});
When("I like 3 random photos", () => {
  ApiPhotoHelpers.getRandomPhoto(3).then(function (response) {
    let respBody = response.body;
    cy.log(respBody);
    for (let i = 0; i < respBody.length; i++) {
      randomPhotoId = respBody[i].id.toString();
      expectLikedId.push(randomPhotoId);
      ApiPhotoHelpers.likeOrUnlikeAPhoto("POST", randomPhotoId);
    }
  });
});

When("I go to Like list page", () => {
  cy.visit(`@${Cypress.env("username")}/likes`);
});
Then("I see the number of likes is 3", () => {
  LikedPhotoListPage.getNumberLikedPhotos().should("have.text", "3");
});
Then("3 photos appear in Likes section", () => {
  ApiUserHelpers.getLikedPhotoList(Cypress.env("username")).then((response) => {
    let respBody = response.body;
    expect(respBody.length).to.equal(3);
    for (let i = 0; i < respBody.length; i++) {
      randomPhotoId = respBody[i].id.toString();
      expect(expectLikedId.includes(randomPhotoId)).to.equal(true);
    }
  });
});

Feature: Update the user in the Profile page
    As a valid user
    I want to update information

    Scenario Outline: Update User Successfully
        Given I logged into the application
        And I go to the Profile page
        When I click on Edit tags link
        And I edit all information of user
        And I click the Update Account button
        And I go to new profile "<new_username>"
        Then I observe that it will take me to the Profile page
        And My full name is displayed as "<your_fullname>"

    Examples:
        | new_username | your_fullname |
        | tunhi058    | Nhi Lien      |
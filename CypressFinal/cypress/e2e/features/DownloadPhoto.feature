Feature: Dowload Photo

    Scenario: Download Successfully
        Given I logged into the application
        When I open a random photo
        And  I download this photo
        Then I notice that the image is downloadable and the correct image has been saved
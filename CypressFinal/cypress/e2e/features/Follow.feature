Feature: Follow

Scenario: Login Successfully
Given I logged into the application
And I click the first photo on home page
When I hover on icon user at the top left corner
And I click the Follow button
Then I observe button background color turn into white and button text turn into Following
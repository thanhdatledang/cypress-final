Feature: Collection Page

    Feature Login page will work depending on the user credentials.

    Background:
        Given There is a private collection with two photos
    Scenario: Remove photos from the collection successfully
        Given I logged into the website
        And I remove 1 photo from the collection
        When I go to collection page
        Then the photo has been removed successfully from the collection
        And there is only 1 remaining photo in the collection

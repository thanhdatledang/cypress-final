Feature: Like 3 random photos
  Scenario: Like 3 random photos in Unsplash
    Given I logged into the application
    And There are no photo in my liked photos list
    When I like 3 random photos
    And I go to Like list page
    Then I see the number of likes is 3
    And 3 photos appear in Likes section
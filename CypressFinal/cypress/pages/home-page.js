// home page
const LNK_USERNAMEPROFILE = "//ul[contains (@class , 'navbar-right')]/li/a"


export const HomePage = {
    getUsernameProfile() {
       return cy.xpath(LNK_USERNAMEPROFILE)
    },
    
}
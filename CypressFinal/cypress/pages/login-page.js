const TXT_EMAIL = "#user_email",
    TXT_PASSWORD = "#user_password",
    BTN_LOGIN = "input[type='submit']";

export const LoginPage = {
    inputEmail(email) {
        cy.get(TXT_EMAIL).type(email);
    },
    inputPassword(password) {
        cy.get(TXT_PASSWORD).type(password);
    },
    clickLoginButton() {
        cy.get(BTN_LOGIN).click();
    },
};

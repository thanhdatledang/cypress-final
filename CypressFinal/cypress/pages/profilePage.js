const BTN_PROFILE_IMG = "#popover-avatar-loggedin-menu-desktop img";
const BTN_VIEW_PROFILE =
  "#popover-avatar-loggedin-menu-desktop ul:nth-child(1) li:nth-child(1) a";
const BTN_EDIT_PROFILE = "a[href='https://unsplash.com/account']";
const TXT_FIRST_NAME = "#user_first_name";
const TXT_LAST_NAME = "#user_last_name";
const TXT_USER_NAME = "#user_username";
const TXT_LOCATION = "#user_location";
const TXA_BIO = "#user_bio";
const BTN_UPDATE_ACCOUNT = "input[value='Update account']";

class ProfilePage {
  static clickProfileImg() {
    cy.get(BTN_PROFILE_IMG).click();
  }
  static clickViewProfile() {
    cy.get(BTN_VIEW_PROFILE).click();
  }
  static clickEditProfile() {
    cy.get(BTN_EDIT_PROFILE).click();
  }
  static inputFirstName(firstName) {
    cy.get(TXT_FIRST_NAME).clear();
    cy.get(TXT_FIRST_NAME).type(firstName);
  }
  static inputLastName(lastName) {
    cy.get(TXT_LAST_NAME).clear();
    cy.get(TXT_LAST_NAME).type(lastName);
  }
  static inputUserName(userName) {
    cy.get(TXT_USER_NAME).clear();
    cy.get(TXT_USER_NAME).type(userName);
  }
  static inputLocation(location) {
    cy.get(TXT_LOCATION).clear();
    cy.get(TXT_LOCATION).type(location);
  }
  static inputBio(bio) {
    cy.get(TXA_BIO).clear();
    cy.get(TXA_BIO).type(bio);
  }
  static clickUpdateAccount() {
    cy.get(BTN_UPDATE_ACCOUNT).click();
  }
  static navigateProfilePage(userName) {
    cy.visit("/@" + userName);
  }
}

export default ProfilePage;
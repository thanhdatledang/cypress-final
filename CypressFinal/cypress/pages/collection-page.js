const LST_ITEM_IN_COLLECTION = "//div[@class='ripi6']";
const IMGS = "//figure[@itemprop='image']"

export const CollectionPage = {

    getItemsInCollection() {
        return cy.xpath(LST_ITEM_IN_COLLECTION)
    },
    goToMyCollection(collectionId) {
        cy.visit(`/collections/${collectionId}`)
    },
    getPhotoCountInCollection() {
        return cy.xpath(IMGS)
    }
}
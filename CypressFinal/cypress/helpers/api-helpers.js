import { ApiConstants } from "../constants/api-constants";

export const ApiUserHelpers = {
  getLikedPhotoList(username) {
    return cy.request({
      method: "GET",
      url: ApiConstants.getLikedPhotoListUrl(username),
      headers: {
        authorization: Cypress.env("token"),
        accept: "application/json",
      },
    });
  },
<<<<<<< HEAD
  getUserToken() {
    cy.request({
      method: "POST",
      url: `https://api.unsplash.com/login`,
      headers: {
        authorization: "Bearer trBloKTaxv31N-V4tgDqyDhNUmL7XDkqsans8-UUDLo",
      },
      body: {
        email: email,
        password: password,
      },
    });
  },
=======
>>>>>>> f433c85cb0404412074cacc91ed1b666d7648359
};
export const ApiPhotoHelpers = {
  likeOrUnlikeAPhoto(method, photoId) {
    cy.request({
      method: method, //method ='POST' is like, 'DELETE' is unlike
      url: ApiConstants.likePhotoUrl(photoId),
      headers: {
        authorization: Cypress.env("token"),
      },
    });
  },
  getRandomPhoto(number) {
    return cy.request({
      method: "GET",
      url: `${ApiConstants.GET_RANDOM_PHOTO_URL}?count=${number}`,
      headers: {
        authorization: Cypress.env("token"),
      },
    });
  },
<<<<<<< HEAD
  dowloadRandomPhoto(photoId) {
    return cy.request({
      method: "GET",
      url: ApiConstants.dowloadPhotoUrl,
      headers: {
        authorization: Cypress.env("token"),
      },
      body: {
        photo_id: photoId,
      },
    });
  },
};

export const ApiCollectionHelpers = {
  createACollection() {
    return cy.request({
      method: "POST",
      url: `https://api.unsplash.com/collections`,
      headers: {
        authorization: Cypress.env("token"),
      },
      body: {
        title: "De Lavie",
        description: "A Collection of my life",
        private: true,
      },
    });
  },
  addAPhotoToCollection(photoId, collectionId) {
    cy.request({
      method: "POST",
      url: ApiConstants.addPhotoUrl(collectionId),
      headers: {
        authorization: Cypress.env("token"),
      },
      body: {
        photo_id: photoId,
      },
    });
  },
  getPhotosFromCollection(collectionId) {
    cy.request({
      method: "GET",
      url: ApiConstants.removeAPhotoUrl(collectionId),
      headers: {
        authorization: Cypress.env("token"),
      },
      body: {
        photo_id: photoId,
      },
    });
  },
  removeAPhotoFromCollection(photoId, collectionId) {
    cy.request({
      method: "DELETE",
      url: ApiConstants.removeAPhotoUrl(collectionId),
      headers: {
        authorization: Cypress.env("token"),
      },
      body: {
        photo_id: photoId,
      },
    });
  },
=======
>>>>>>> f433c85cb0404412074cacc91ed1b666d7648359
};

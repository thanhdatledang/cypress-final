export const ApiConstants = {
  //end point
  GET_RANDOM_PHOTO_URL: `${Cypress.env("apiUrl")}/photos/random`,
  LIKE_PHOTO_URL: "/photos/:id/like",
  likePhotoUrl(id) {
    return `${Cypress.env("apiUrl")}/photos/${id}/like`;
  },
  getLikedPhotoListUrl(username) {
    return `${Cypress.env("apiUrl")}/users/${username}/likes`;
  },

  addPhotoUrl(collectionId){
    return `${Cypress.env("apiUrl")}/collections/${collectionId}/add`;
  },
  removeAPhotoUrl(collectionId){
    return `${Cypress.env("apiUrl")}/collections/${collectionId}/remove`;
  },
  getPhotosUrl(collectionId){
    return `${Cypress.env("apiUrl")}/collections/${collectionId}/photos`;
  },
  dowloadPhotoUrl(photoId){
    return `${Cypress.env("apiUrl")}/photos/${photoId}/download`;
  },

};
